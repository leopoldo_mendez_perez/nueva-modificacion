import { Storage } from '@ionic/storage';
import { Component, NgZone, OnInit, ViewChildren } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { RestService } from '../services/rest.service';
import { Router } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

declare var google:any;

interface Marker {
  position: {
    lat: number,
    lng: number,
  };
  title: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  status:any;
  ubicacion:any={};
  Position:any;
  Items:any;
  us:any;
  resultado:any;
  private googleAutocomplete=new google.maps.places.AutocompleteService();
  public searchResult=new Array<any>();

  directionsService = new google.maps.DirectionsService();
directionsDisplay = new google.maps.DirectionsRenderer();
origin = { lat: 14.9069678, lng: -91.6575352 };
message:string=null;
file:string=null;
link:string=null;
subject:string=null;
idUbicacion:any;
cordx:any;
cordy:any;
// Concepcion chiquirichapa

  constructor(private http: HttpClient,private storage: Storage,private socialSharing: SocialSharing,private geolocation:Geolocation,private ngZone:NgZone,public proveedor:RestService,private router: Router) {
    }
  public search:string='';

 map=null;
  addMarkersTopMap(markers){

  }
  ngOnInit() {
    console.log("HOla");
    this.loadMap();
  }
  async ionViewDidLeave(){
      //busqueda de usuario almacenado en la variable usuario
      const storage = await this.storage.create();
    this.us= await storage.get('viaje');
    console.log(this.us);
   if(this.us!="fin"){
  
        //Inicio de Viaje
       
        await new Promise(f => setTimeout(f,2000));
    
  
      console.log(this.ubicacion.coordenadax);
      console.log(this.ubicacion.coordenaday);
  //Busqueda de idCoordenada por cordenada x y y
  this.proveedor.ConsultarIdUbicacion(this.ubicacion.cordenadax,this.ubicacion.cordenaday).subscribe(async (res:any)=>{
    console.log("Ubicacion");
    console.log(res[0].idubicacion);
    const idUbicacion = await storage.set('idUbicacion',res[0].idubicacion);   
    console.log("IdUbicacion en archivo");
    console.log(idUbicacion);
        
    });
   
   }else{
     console.log("No tiene iniciado el viaje");
   }
while (this.us!="fin") {
  //Actualizacion de coordenadas 
  await new Promise(f => setTimeout(f, 2000));
  //obtencion de localizacoin 
this.geolocation.getCurrentPosition().then((geoposition:Geoposition)=>{
  const indicatorsEle: HTMLElement = document.getElementById('indicators');
this.cordx=geoposition.coords.longitude;
this.cordy=geoposition.coords.latitude;
this.ubicacion.cordenadax=this.cordx;
 this.ubicacion.cordenaday=this.cordy;
console.log(this.cordx);
console.log(this.cordy);
})

  await new Promise(f => setTimeout(f, 2000));
console.log("Hola");
this.us= await storage.get('viaje');
  //registro Se manda a modificar la coordenada----------------------------------------
  this.createServiceUpdate(this.cordx,this.cordy,await storage.get('idUbicacion'));
}
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then((geoposition:Geoposition)=>{
      const indicatorsEle: HTMLElement = document.getElementById('indicators');

      var long= geoposition.coords.longitude;
      var lat= geoposition.coords.latitude;
      this.ubicacion.cordenadax=geoposition.coords.longitude;
      this.ubicacion.cordenaday=geoposition.coords.latitude;
       // create a new map by passing HTMLElement
    const mapEle: HTMLElement = document.getElementById('map');
    // create LatLng object
    const myLatLng = {lat:lat, lng: long};
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: this.origin,
      zoom: 12
    });
    this.directionsDisplay.setMap(this.map);
    this.directionsDisplay.setPanel(indicatorsEle);

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
      const marker={
        position:{
          lat:lat,
          lng:long,
        },
        title:'punto'
      }
      this.addMarker(marker);
    });
    })
  }
  private calculateRoute(org,des){

    //codigo para calcular la ruta
    this.directionsService.route({
      origin: org,
      destination: des,
      travelMode: google.maps.TravelMode.DRIVING,
    }, (response, status)  => {
      if (status === google.maps.DirectionsStatus.OK) {
        this.directionsDisplay.setDirections(response);
      } else {
        alert('Could not display directions due to: ' + status);
      }
    });
  }
 addMarker(marker: Marker) {
 this.Position=marker.position
    return new google.maps.Marker({
      position: marker.position,
      map: this.map,
      title: marker.title,
      snippet:'Come and find me!'
    });
  }
  searchChanged(){
if(!this.search.trim().length) return;
this.googleAutocomplete.getPlacePredictions({input:this.search},predictions=>{
  this.ngZone.run(()=>{
    this.searchResult=predictions;

  });
console.log(this.searchResult);
});
 }
 async calcRoute(item:any){
  this.search='';

  this.search='';

this.proveedor.loadInfo(item.description).subscribe((res:any)=>{
 console.log(res.results[0].geometry.location);
 console.log(this.Position);
 this.calculateRoute(this.Position,res.results[0].geometry.location);

});

}

//regresar al menu principal
return(){
  this.router.navigate(['/menu'])
}
share(){
  
this.socialSharing.share("Hola",null,"mi archivo","lik");
}

async finviaje(){
  this.status=false;
  //Estado de boton
  
  //busqueda de usuario almacenado en la variable usuario
  const storage = await this.storage.create();
  const ingreso = await storage.set('viaje',"fin");  
console.log(ingreso);
this.router.navigate(['/registro-punteo'])
}
async CrearViaje(){
  this.status=true;
      //busqueda de usuario almacenado en la variable usuario
      const storage = await this.storage.create();
      this.us = await storage.set('viaje',"inicio");  
     console.log(this.us);
    
}
//--------------------------------------------------------------------------------

  modificar(){

}

iniciarViaje(){
  this.status=true;
  //registro de usuario----------------------------------------
  this.createServiceAlmacenUbicacion().subscribe(
    data=>this. confirmarAlmacenamiento(data));
    this.router.navigate(['/realizar-viaje'])
}

  //confirmacion-------------------------------------------
  confirmarAlmacenamiento(resultado:any){
    if(resultado){
    console.log("Ok ↖(^▽^)↗");
    }else{
      console.log("Error");
  }
    }
    createServiceAlmacenUbicacion(){
        if(this.ubicacion==""){
          console.log(this.ubicacion);
        }else{
          var httpOptions={
            headers:new HttpHeaders({
              'Content-Type':'application/json'
            })
          }
          return this.http.post<any>("http://localhost:3033/ubicacion/guardar",this.ubicacion,httpOptions);
        }
      }
//-----------------------------------UPDATE

    createServiceUpdate(cordenadax,cordenaday,idubicacion){
      console.log("Update Listo ↖(^▽^)↗");
  
      return this.http.post("http://localhost:3033/ubicacion/update/"+cordenadax+"/"+cordenaday+"/"+idubicacion,HttpHeaders).toPromise();
    }
        
      
} 
