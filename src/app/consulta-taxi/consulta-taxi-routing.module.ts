import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsultaTaxiPage } from './consulta-taxi.page';

const routes: Routes = [
  {
    path: '',
    component: ConsultaTaxiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultaTaxiPageRoutingModule {}
