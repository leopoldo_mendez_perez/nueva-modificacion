import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsultaTaxiPageRoutingModule } from './consulta-taxi-routing.module';

import { ConsultaTaxiPage } from './consulta-taxi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsultaTaxiPageRoutingModule
  ],
  declarations: [ConsultaTaxiPage]
})
export class ConsultaTaxiPageModule {}
