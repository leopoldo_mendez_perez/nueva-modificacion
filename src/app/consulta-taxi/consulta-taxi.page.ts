import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NgZone,  ViewChildren } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { RestService } from '../services/rest.service';
import { Router } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-consulta-taxi',
  templateUrl: './consulta-taxi.page.html',
  styleUrls: ['./consulta-taxi.page.scss'],
})
export class ConsultaTaxiPage implements OnInit {
  vehiculo:any={};
  taxista:any={};
  fecha:any;
  hora:any;
  idTaxista:any;
  idDatosPersonas:any;
  login:any;
  idCliente:any;
  id_vehiculo:any;
  ubicacion:any;
  identificadores:any={};
  validacion:any;
  constructor(public loadingController: LoadingController,public toastController: ToastController,private http: HttpClient,private storage: Storage,private socialSharing: SocialSharing,private geolocation:Geolocation,private ngZone:NgZone,public proveedor:RestService,private router: Router) {
  }

  ngOnInit() {

  }
  return(){
  }
  realizarBusqueda(){
    this.consultarIdUbicacion();
    this.consultaFechaHora();
    this.consultaCliente();
    if(this.vehiculo.buscarplaca!=null){
  //Realizar Busqueda por placa
  this.proveedor.ConsultarVehiculoPorPlaca(this.vehiculo.buscarplaca).subscribe(async (result:any)=>{
    console.log(result);
    this.id_vehiculo=result[0].idvehiculo;
    this.identificadores.id1=result[0].idvehiculo;
    this.vehiculo.placa=result[0].numeroplaca;
    this.vehiculo.color=result[0].color;
    this.vehiculo.marca=result[0].marca;
    this.vehiculo.autorizacion=result[0].numero_autorizacion;
    this.idTaxista=result[0].operador_del_taxi;
    const storage=await this.storage.create();
    await storage.set('taxista',result[0].operador_del_taxi);  
    this.consultaOperadorTaxi();
     });
    }
  }


//consulta hora y fecha
  async consultaFechaHora(){
  //consulta de datos de persona
 while(this.validacion!="fin"){
  await new Promise(f => setTimeout(f, 1000));
  this.proveedor.Consultarfechahora().subscribe(async (result:any)=>{
    this.identificadores.hora=result.hora;
  this.fecha=result.fecha;
  this.hora=result.hora;
  console.log(this.hora);
  });
 }
    }
//consulta de vehiculo por placa
  consultaVehiculo(){
  console.log("Hoal");
    }
//consulta de operador de taxi
consultaOperadorTaxi(){
  this.proveedor.ConsultarOperadorTaxi(this.idTaxista).subscribe(async (result:any)=>{
    this.idDatosPersonas=result[0].datos_personales1;
    this.taxista.licencia=result[0].tipo_licencia;
  console.log(result);
   this.consultaDatosOperador();
    });
    }
//consulta de datos del operador
consultaDatosOperador(){
  this.proveedor.ConsultarDatosPersona(this.idDatosPersonas).subscribe(async (result:any)=>{
    this.taxista.nombre=result[0].nombre;
    this.taxista.apellido=result[0].apellido;
  });   
 }

//consultar cliente por id
async consultaCliente(){
  const storage = await this.storage.create();
   this.login = await storage.get('loginId');
  console.log( this.login);
 this.proveedor.ConsultarIdCliente(this.login).subscribe(async (result:any)=>{
    this.idCliente=result[0].idcliente;
    console.log(this.idCliente);

  });   
    }
//consultar login por usuario y contrasenia

async consultarIdUbicacion(){
  const storage = await this.storage.create();
  this.ubicacion = await storage.get('idUbicacion');
 console.log( this.ubicacion);
}

registro_Informacion(){

}

   
}
