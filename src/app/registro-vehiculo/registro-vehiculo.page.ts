import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { RestService } from '../services/rest.service';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-registro-vehiculo',
  templateUrl: './registro-vehiculo.page.html',
  styleUrls: ['./registro-vehiculo.page.scss'],
})
export class RegistroVehiculoPage implements OnInit {
vehiculo:any={};
operador:String="";
constructor(public loadingController: LoadingController,private http: HttpClient,private router: Router, private storage: Storage,public toastController: ToastController,public proveedor:RestService) { }


  ngOnInit() {
  }
 async register(){
    const storage = await this.storage.create();
    const login = await storage.get('login');
      //consulta de id de usuario
      this.proveedor.ConsultaTaxista(login).subscribe(async (res:any)=>{
        console.log(res[0].idtaxista);
        console.log("Usuario");
        console.log(res[0].idtaxista);
        this.vehiculo.operador_del_taxi=res[0].idtaxista;
        });
        this.cargando(3000);//cargando
        await new Promise(f => setTimeout(f,3000));
        //Almacenamiento en la base de datos
        this.createService().subscribe(
          data=>this.confirmar(data));
          this.router.navigate(['/principal'])
  }
   //confirmacion-------------------------------------------
   confirmar(resultado:any){
    if(resultado){
      this.mensaje("Ok \(◦'⌣'◦)/");
    }else{
      this.mensaje("Error");
    }
    }
  //creacion de nuevo usuario------------------------------------
  createService(){
    if(this.vehiculo==""){
      console.log(this.vehiculo);
    }else{
      var httpOptions={
        headers:new HttpHeaders({
          'Content-Type':'application/json'
        })
      }
      return this.http.post<any>("http://localhost:3033/vehiculo/guardar",this.vehiculo,httpOptions);
    }
  }
  async  mensaje(mensaje){
    //se muestra el mensaje con toast
    const listo =await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    listo.present();

 }
 public  async cargando(tiempo){
  const loading = await this.loadingController.create({
    cssClass: 'my-custom-class',
    message: 'Procesando...',
    duration: tiempo
  });
  await loading.present();

 }
}
