import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
       { title: 'Ingresar', url: '/login-user', icon: 'person-add' },
    { title: 'Registrarse', url: '/nuevo-login', icon: 'person-add' },
  ];
 // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() {}
}
