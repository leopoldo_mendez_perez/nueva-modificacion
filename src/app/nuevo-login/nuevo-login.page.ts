import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-nuevo-login',
  templateUrl: './nuevo-login.page.html',
  styleUrls: ['./nuevo-login.page.scss'],
})
export class NuevoLoginPage implements OnInit {
  private _storage: Storage | null = null;

  login:any={};
  loading:boolean=false;
  contra:string="";
  nuevacontrasenia:string="";
  constructor(public loadingController: LoadingController,private router: Router,private http: HttpClient,public toastController: ToastController, private storage: Storage) { }

  ngOnInit() {
  }
 async crear(){
if(this.login.contrasenia==this.contra){
  //almacenamiento de usuario

  const storage = await this.storage.create();
  const user = await storage.set('usuario',this.login.usuario);   
  console.log(user); 


  //codoigo de creacion de usuario
  let formulario:any=document.getElementById("register"); 
 this.createService().subscribe(
data=>this.confirmar(data))

this.cargando(5000);//cargando

this.mensaje("Ok ↖(^▽^)↗");
  //redireccion a la otra pagina
  //eliminacion de datos en los campos
  this.login.contrasenia="";
  this.contra="";
  this.router.navigate(['/register'])
}else{
  //codigo de mostrar error
  const mensajeError = await this.toastController.create({
    message: ' "Uno de las constraseñas no coincide"',
    duration: 500
  });
  //se muestra mensaje de error
  mensajeError.present();
  console.log("Uno de las constraseñas no coincide");
  //eliminacion de datos en los campos
this.login.contrasenia="";
this.contra=""
}   
 
  }
  confirmar(resultado:any){
  if(resultado){
    this.mensaje("Ok ↖(^▽^)↗");
  this.login={};
  }else{
  alert("Error al crear usuario");
}
  }
  createService(){
    if(this.login==""){
      console.log(this.login);
    }else{
      var httpOptions={
        headers:new HttpHeaders({
          'Content-Type':'application/json'
        })
      }
      return this.http.post<any>("http://localhost:3033/login/guardar",this.login,httpOptions);
    }
  }
  public  async cargando(tiempo){
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Procesando...',
      duration: tiempo
    });
    await loading.present();
  
   }
   async  mensaje(mensaje){
    //se muestra el mensaje con toast
    const listo =await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    listo.present();

 }
}
