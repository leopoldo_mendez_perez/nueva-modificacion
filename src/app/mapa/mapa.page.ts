import { AfterContentInit, Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
declare var google;
@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})

export class MapaPage implements OnInit, AfterContentInit {
  map;
@ViewChild('mapElement') mapElement;
  constructor() { }

  ngOnInit() {
  }

ngAfterContentInit(): void {
  this.map=new google.maps.Map(
    this.mapElement.nativeElement,
    {
      center: {lat: -34.397, lng:150.644},
      zoom:8
  });
}
}
