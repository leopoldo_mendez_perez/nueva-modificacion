import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RealizarViajePage } from './realizar-viaje.page';

const routes: Routes = [
  {
    path: '',
    component: RealizarViajePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RealizarViajePageRoutingModule {}
