import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(public http: HttpClient) {
  }
  loadInfo(item){
  return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBeEIqzDPhyR2ekK0-XpEWj1MmWJKw_h9k&address='+item);
 }
 validacion(usuario,contrasenia){
  return this.http.get('http://localhost:3033/login/'+usuario+'/'+contrasenia);
 }
 extraerCodigoUsuario(usuario){
  return this.http.get("http://localhost:3033/login/consulta/"+usuario);
 }
 loginConsultar(item){
  return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBeEIqzDPhyR2ekK0-XpEWj1MmWJKw_h9k&address='+item);
 }
 RegistroUsuario(item){
  return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBeEIqzDPhyR2ekK0-XpEWj1MmWJKw_h9k&address='+item);
 }
 //Api para consultar datos para usuario cliente y taxista
 consultarDatos(nombre,telefono){
  return this.http.get('http://localhost:3033/datos/consulta/'+nombre+"/"+telefono);
 }
  //Api para consultar datos para usuario cliente y taxista
  ConsultaTaxista(login){
    return this.http.get('http://localhost:3033/taxista/consulta/'+login);
   }
   Consultarfechahora(){
    return this.http.get('http://localhost:3033/fechahora/buscar');
   }
   ConsultarIdUbicacion(cordenadax,cordenaday){
    return this.http.get('http://localhost:3033/ubicacion/cordenada/'+cordenadax+"/"+cordenaday);
   }
   //consultar vehiculo por placa
   ConsultarVehiculoPorPlaca(placa){
    return this.http.get('http://localhost:3033/vehiculo/consulta/'+placa);
   }
      //OperadorTaxi
   ConsultarOperadorTaxi(idOperador){
    return this.http.get('http://localhost:3033/taxista/consultaTaxista/'+idOperador);
   }
       //OperadorTaxi
       ConsultarDatosPersona(idDatosPersona){
        return this.http.get('http://localhost:3033/datos/consultaDatos/'+idDatosPersona);
       }
   //OperadorTaxi
   ConsultaLogin(idDatosPersona){
    return this.http.get('http://localhost:3033/datos/consultaDatos/'+idDatosPersona);
   }
     //OperadorTaxi
     ConsultaIdLogin(usuario,contrasenia){
      return this.http.get('http://localhost:3033/login/consultaLogin/'+usuario+"/"+contrasenia);
     }
      //Con
      ConsultarIdCliente(idLogin){
        return this.http.get("http://localhost:3033/cliente/consultaCliente/"+idLogin);
       }
}
