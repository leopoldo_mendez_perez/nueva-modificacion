import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterTaxistaPageRoutingModule } from './register-taxista-routing.module';

import { RegisterTaxistaPage } from './register-taxista.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterTaxistaPageRoutingModule
  ],
  declarations: [RegisterTaxistaPage]
})
export class RegisterTaxistaPageModule {}
