import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { RestService } from '../services/rest.service';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-register-taxista',
  templateUrl: './register-taxista.page.html',
  styleUrls: ['./register-taxista.page.scss'],
})
export class RegisterTaxistaPage implements OnInit {
usuario_taxista:any={};
registro_taxista:any={};
  loading:boolean=false;
  numero:any;
  private _storage: Storage | null = null;
  constructor(public loadingController: LoadingController,private http: HttpClient,private router: Router,public toastController: ToastController,public proveedor:RestService, private storage: Storage) { }

 async ngOnInit() {

  }


  goToHome(){
    this.router.navigate(['/home'])
    }
    user(){
      this.router.navigate(['/register'])
      }
    taxista(){
      this.router.navigate(['/register-taxista'])
      }
     async register(){
    
   //registro de usuario----------------------------------------
   this.createService().subscribe(
    data=>this.confirmar(data));
   
//busqueda de usuario almacenado en la variable usuario
         const storage = await this.storage.create();
         const us = await storage.get('usuario');
         console.log("Usuario");
         console.log(us);

         //registro de usuario y telefono
         const nombre = await storage.set('nombre', this.usuario_taxista.nombre);   
         const telefono = await storage.set('telefono',this.usuario_taxista.telefono);   
         console.log(nombre); 
         console.log(telefono);

      //consulta de id de usuario
      this.proveedor.extraerCodigoUsuario(us).subscribe(async (res:any)=>{
        console.log(res[0].idlogin);
        this.registro_taxista.login1=res[0].idlogin;
        const login = await storage.set('login',res[0].idlogin);   
        console.log("Id_Usruario===================================");
            
        });
  
//      await new Promise(f => setTimeout(f, 10000));
 
//this.cargando(7000);//cargando
//await new Promise(f => setTimeout(f, 7000));
  //consulta de datos de persona
  this.proveedor.consultarDatos(this.usuario_taxista.nombre,this.usuario_taxista.telefono).subscribe(async (result:any)=>{
    console.log("Id datos personales");
    this.registro_taxista.datos_personales1=result[0].iddatos;
    console.log(result[0].iddatos);
    this.numero=result[0].iddatos;
    
  });
  
 //-------------------------------------------------------------d

 this.cargando(8000);//cargando
 await new Promise(f => setTimeout(f,8000));
  this.createServiceTaxista().subscribe(
  data=>this.confirmarTaxista(data));
 //redireccion a la otra pagina
 this.mensaje("Usuario Creado Exitosamente");
 console.log(this.createServiceTaxista());

      }

      //confirmacion-------------------------------------------
      confirmar(resultado:any){
        if(resultado){
          this.mensaje("Ok \(◦'⌣'◦)/");
        }else{
          this.mensaje("Error");
        }
        }
      //creacion de nuevo usuario------------------------------------
      createService(){
        if(this.usuario_taxista==""){
          console.log(this.usuario_taxista);
        }else{
          var httpOptions={
            headers:new HttpHeaders({
              'Content-Type':'application/json'
            })
          }
          return this.http.post<any>("http://localhost:3033/datos/guardar",this.usuario_taxista,httpOptions);
        }
      }
  //confirmacion-------------------------------------------
  confirmarTaxista(resultado:any){
    if(resultado){
    this.mensaje("Ok ↖(^▽^)↗");
     //almacenamiento de 
 this.router.navigate(['/registro-vehiculo']);
    }else{
      this.mensaje("Error");
  }
    }
    createServiceTaxista(){
        if(this.registro_taxista==""){
          console.log(this.registro_taxista);
        }else{
          var httpOptions={
            headers:new HttpHeaders({
              'Content-Type':'application/json'
            })
          }
          return this.http.post<any>("http://localhost:3033/taxista/guardar",this.registro_taxista,httpOptions);
        }
      }
public  async cargando(tiempo){
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Procesando...',
      duration: tiempo
    });
    await loading.present();
  
   }
 async  mensaje(mensaje){
      //se muestra el mensaje con toast
      const listo =await this.toastController.create({
        message: mensaje,
        duration: 2000
      });
      listo.present();

   }
}
