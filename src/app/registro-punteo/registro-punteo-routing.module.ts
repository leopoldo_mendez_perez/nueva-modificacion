import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistroPunteoPage } from './registro-punteo.page';

const routes: Routes = [
  {
    path: '',
    component: RegistroPunteoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistroPunteoPageRoutingModule {}
