import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { RestService } from '../services/rest.service';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-registro-punteo',
  templateUrl: './registro-punteo.page.html',
  styleUrls: ['./registro-punteo.page.scss'],
})

export class RegistroPunteoPage implements OnInit {
  picToView:String="";
  usuario:string="";
  registrarPunteo:any={};
  
  constructor(public loadingController: LoadingController,private http: HttpClient,private router: Router,public toastController: ToastController,public proveedor:RestService, private storage: Storage) { }

  private visible = []; 
  async ngOnInit() {
 
    this.picToView="./assets/imagenes/tax.jpg";
    const storage = await this.storage.create();
   this.registrarPunteo.usuario=await storage.get('taxista');
  }
  
  public punteo(punteo){
    this.registrarPunteo.calificacion=punteo;
console.log(punteo);
  }
  changeView(parm) {
   switch(parm){
     case 1:
      this.picToView="./assets/imagenes/triste.gif";
     break;
     case 2:
        this.picToView="./assets/imagenes/mediofeliz.gif";
     break;
       case 3:
          this.picToView="./assets/imagenes/bien.gif";
      break;
      case 4:
        this.picToView="./assets/imagenes/feliz.gif";
    break;
    case 5:
      this.picToView="./assets/imagenes/muyfeliz.gif";
  break;
   }
  }  
 
  register(){
 //registro de usuario----------------------------------------
 this.createServicePunteo().subscribe(
  data=>this.confirmarPuteo(data));
  }
  isReadonly() {
    return this.isReadonly;   //return true/false 
  }
  return(){
    this.router.navigate(['/menu'])
  }
    //confirmacion-------------------------------------------
    confirmarPuteo(resultado:any){
      if(resultado){
      this.mensaje("Ok ↖(^▽^)↗");
       //almacenamiento de 
   this.router.navigate(['/menu']);
      }else{
        this.mensaje("Error");
    }
      }
      createServicePunteo(){
          if(this.registrarPunteo==""){
            console.log(this.registrarPunteo);
            this.router.navigate(['/menu'])

          }else{
            var httpOptions={
              headers:new HttpHeaders({
                'Content-Type':'application/json'
              })
            }
            return this.http.post<any>("http://localhost:3033/calificacion/guardar",this.registrarPunteo,httpOptions);
          }
        }
        async  mensaje(mensaje){
          //se muestra el mensaje con toast
          const listo =await this.toastController.create({
            message: mensaje,
            duration: 2000
          });
          listo.present();
    
       }
}
