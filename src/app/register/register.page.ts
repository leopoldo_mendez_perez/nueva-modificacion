import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { RestService } from '../services/rest.service';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  usuario:any={};
  codigo:any={};
  loading:boolean=false;
  
  constructor(public loadingController: LoadingController,private http: HttpClient,private router: Router, private storage: Storage,public toastController: ToastController,public proveedor:RestService) { }

  ngOnInit() {
  }
  goToHome(){
    this.router.navigate(['/home'])
    }
    user(){
      this.router.navigate(['/register'])
      }
    taxista(){
      this.router.navigate(['/register-taxista'])
      }
     async register(){
       

  //registro de usuario----------------------------------------
  
  let formulario:any=document.getElementById("register"); 
  this.createService().subscribe(
 data=>this.confirmar(data));
 this.cargando(500);//cargando

//busqueda de usuario almacenado en la variable usuario
         const storage = await this.storage.create();
         const us = await storage.get('usuario');
         console.log("Usuario");
         console.log(us);
      //consulta de id de usuario
      this.proveedor.extraerCodigoUsuario(us).subscribe(async (res:any)=>{
        console.log(res[0].idlogin);
        this.codigo.login=res[0].idlogin;
        console.log("Id_Usruario");
            
        });

         
//      await new Promise(f => setTimeout(f, 10000));
 
      console.log("Datos de persona");
      console.log(this.usuario.nombre);
      console.log(this.usuario.apellido);
      console.log(this.usuario.telefono);
      this.cargando(7000);//cargando
      await new Promise(f => setTimeout(f, 4000));
        //consulta de datos de persona
        this.proveedor.consultarDatos(this.usuario.nombre,this.usuario.telefono).subscribe(async (result:any)=>{
          console.log("Id datos personales");
      
          this.codigo.datos_personales=result[0].iddatos;
          console.log(result[0].iddatos);
        });
      
       //-------------------------------------------------------------d
       //await new Promise(f => setTimeout(f, 10000));
       this.cargando(7000);//cargando
       await new Promise(f => setTimeout(f, 7000));

        this.createServiceCliente().subscribe(
        data=>this.confirmarCliente(data));

       this.mensaje("Usuario Creado Exitosamente");
       console.log(this.createServiceCliente());
       this.router.navigate(['/principal'])
      }
      confirmarCliente(resultado:any){
        if(resultado){
        this.mensaje("Ok ↖(^▽^)↗");
        }else{
          this.mensaje("Error");
      }
        }
        createServiceCliente(){
            if(this.codigo==""){
              console.log(this.codigo);
            }else{
              var httpOptions={
                headers:new HttpHeaders({
                  'Content-Type':'application/json'
                })
              }
              return this.http.post<any>("http://localhost:3033/cliente/guardar",this.codigo,httpOptions);
            }
          }
      //confirmacion-------------------------------------------
      confirmar(resultado:any){
        if(resultado){
        alert("Usuario Crado");
      
        }else{
        alert("Error al crear usuario");
      }
        }
      //creacion de nuevo usuario------------------------------------
      createService(){
        if(this.usuario==""){
          console.log(this.usuario);
        }else{
          var httpOptions={
            headers:new HttpHeaders({
              'Content-Type':'application/json'
            })
          }
          return this.http.post<any>("http://localhost:3033/datos/guardar",this.usuario,httpOptions);
        }
      }
      public  async cargando(tiempo){
        const loading = await this.loadingController.create({
          cssClass: 'my-custom-class',
          message: 'Procesando...',
          duration: tiempo
        });
        await loading.present();
      
       }
     async  mensaje(mensaje){
          //se muestra el mensaje con toast
          const listo =await this.toastController.create({
            message: mensaje,
            duration: 2000
          });
          listo.present();
    
       }
       
}
