import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-viajes-realizadas',
  templateUrl: './viajes-realizadas.page.html',
  styleUrls: ['./viajes-realizadas.page.scss'],
})
export class ViajesRealizadasPage implements OnInit {
  constructor(private router: Router) { }

  ngOnInit() {
  }
  click(){
    this.router.navigate(['/principal'])
  };
  //regresar al menu principal
return(){
  this.router.navigate(['/menu'])
}
}
