import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from '../services/rest.service';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingController } from '@ionic/angular';
import { NativeAudio } from '@ionic-native/native-audio/ngx';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.page.html',
  styleUrls: ['./login-user.page.scss'],
})
export class LoginUserPage implements OnInit {
  private _storage: Storage | null = null;

usuario:String="";
contrasenia:String="";
  constructor(public loadingController: LoadingController,private router: Router,public proveedor:RestService,public toastController: ToastController,
    private storage: Storage,private nativeAudio: NativeAudio) {
    
    }

  ngOnInit() {

  }
goToHome(){
}

async logearse(){
const storage = await this.storage.create();
this._storage = storage;
this._storage?.set('validacion','acceso');

const name = await storage.get('validacion');
console.log(name);

  const toast = await this.toastController.create({
    message: 'Usuaro / contraseña Incorrecta ()',
    duration: 2000
   
  });
   //consultar usuario por usuario y contrasenia
  this.proveedor.ConsultaIdLogin(this.usuario,this.contrasenia).subscribe(async (result:any)=>{
    this._storage?.set('loginId',result[0].idlogin);
 console.log(result[0].idlogin);
  }); 
  console.log(this.usuario);
  console.log(this.contrasenia);
if(this.usuario==''||this.contrasenia==''){

    const mensajeError = await this.toastController.create({
      message: '   llenar cada campo  ಥ_ಥ',
      duration: 2000
    });
    this.cargando(2000);
    mensajeError.present();
}else{
  this.proveedor.validacion(this.usuario,this.contrasenia).subscribe(async (res:any)=>{
    if(res.Estado=='Error'){
      console.log(res.Estado);
      toast.present();
      this.cargando(500);
      this.usuario="";
      this.contrasenia="";
    }else{
      this.usuario="";
      this.contrasenia="";
      this.cargando(500);
      this.router.navigate(['/menu'])
      const listo =await this.toastController.create({
        message: 'Bienvenido  (｡◕‿◕｡) ',
        duration: 500
      });
      listo.present();

    }
    });
}
}
public  async cargando(tiempo){
  const loading = await this.loadingController.create({
    cssClass: 'my-custom-class',
    message: 'Procesando...',
    duration: tiempo
  });
  await loading.present();
 }
}
